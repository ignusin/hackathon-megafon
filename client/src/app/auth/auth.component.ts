import { Component } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthService } from "../services/auth.service";

@Component({
    selector: 'auth-component',
    templateUrl: './auth.component.html',
    styleUrls: [ './auth.component.css' ]
})
export class AuthComponent {
    private _authService: AuthService;
    
    constructor(authService: AuthService) {
        this._authService = authService;
    }

    signInForm: FormGroup = new FormGroup({
        username: new FormControl('', [ Validators.required ]),
        password: new FormControl('', [ Validators.required ])
    });

    onSubmit() {
        const username = this.signInForm.get('username').value;
        const password = this.signInForm.get('password').value

        this._authService
            .signIn(username, password);
    }
}