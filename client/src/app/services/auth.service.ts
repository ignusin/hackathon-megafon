import { Injectable } from "@angular/core";
import { AuthRestService } from "./auth.rest.service";
import { Observable, Subject } from "rxjs";

const setCookie = function (name, value, days?) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

const getCookie = function (name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
};

const eraseCookie = function (name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}

@Injectable()
export class AuthService {
    private _authRestService: AuthRestService;

    constructor(authRestService: AuthRestService) {
        this._authRestService = authRestService;
    }

    signIn(username, password): Observable<Boolean> {
        const result = new Subject<Boolean>();

        this._authRestService
            .authenticate(username, password)
            .subscribe(
                x => {
                    this.setAuthCookie(x.id);
                    result.next(true);
                },
                x => result.next(false)
            );

        return result;
    }

    signOut() { 
        this.clearAuthCookie();
    }

    get userId() {
        return parseInt(getCookie('userId'));
    }

    private setAuthCookie(userId) {
        setCookie('userId', userId);
    }

    private clearAuthCookie() {
        eraseCookie('userId');
    }
}