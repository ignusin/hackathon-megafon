import { Observable, Subject } from "rxjs";
import { Injectable } from "@angular/core";
import { IAuthInfo } from "../objects";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class AuthRestService {
    private _httpClient: HttpClient;

    constructor(httpClient: HttpClient) {
        this._httpClient = httpClient;
    }

    authenticate(username, password): Observable<IAuthInfo> {
        return this._httpClient
            .post<IAuthInfo>(
                '/api/auth',
                { username: username, password: password }
            );
    }
}