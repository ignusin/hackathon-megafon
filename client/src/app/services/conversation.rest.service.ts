import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { IConversationInfo } from "../objects";

@Injectable()
export class ConversationRestService {
    private _httpClient: HttpClient;

    constructor(httpClient: HttpClient) {
        this._httpClient = httpClient;
    }

    getConversations(): Observable<IConversationInfo[]> {
        return this._httpClient
            .get<IConversationInfo[]>('/api/conversations');
    }
}
