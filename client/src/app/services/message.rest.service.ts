import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { IMessageInfo } from "../objects";
import { Observable } from "rxjs";

@Injectable()
export class MessageRestService {
    private _httpClient: HttpClient;

    constructor(httpClient: HttpClient) {
        this._httpClient = httpClient;
    }

    addMessage(cId: number, msg: string): Observable<Object> {
        return this._httpClient
            .post<Object>('/api/messages/' + cId, { message: msg });
    }

    getMessages(cId: number): Observable<IMessageInfo[]> {
        return this._httpClient
            .get<IMessageInfo[]>('/api/messages/' + cId);
    }
}
