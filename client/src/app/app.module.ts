import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { HistoryListComponent } from './main/history/history-list.component';
import { MainComponent } from './main/main.component';
import { MessagesComponent } from './main/messages/messages.component';
import { MergeFormComponent } from './main/merge/merge-form.component';
import { RequestListComponent } from './main/requests/request-list.component';
import { SimilarListComponent } from './main/similar/similar-list.component';

import { AuthRestService } from './services/auth.rest.service';
import { AuthService } from './services/auth.service';
import { ConversationRestService } from './services/conversation.rest.service';
import { SettingFormComponent } from './main/settings/settings-form.component';
import { MessageRestService } from './services/message.rest.service';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    HistoryListComponent,
    MainComponent,
    MessagesComponent,
    MergeFormComponent,
    RequestListComponent,
    SettingFormComponent,
    SimilarListComponent
  ],
  entryComponents: [
    MergeFormComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  providers: [
    AuthService,
    AuthRestService,
    ConversationRestService,
    MessageRestService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
