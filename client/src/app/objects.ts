export interface IAuthInfo {
    id: number
}

export const RequestStatus = {
    open: 1,
    closed: 2
}

export interface IConversationInfo {
    id: number,
    authorId: number,
    authorName: string,
    time: number,
    text: string
}

export interface IMessageInfo {
    id: number,
    authorId: number,
    authorName: string,
    time: number,
    timeString?: string,
    text: string
}
