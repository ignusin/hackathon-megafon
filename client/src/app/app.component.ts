import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private _authService: AuthService;

  constructor(authService: AuthService) {
    this._authService = authService;
  }

  isAuthenticated() {
    return !!this._authService.userId;
  }

}
