import { Component, OnInit } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { ConversationRestService } from "../services/conversation.rest.service";
import { IConversationInfo } from "../objects";

@Component({
    selector: 'main-component',
    templateUrl: './main.component.html',
    styleUrls: [ './main.component.css' ]
})
export class MainComponent implements OnInit {
    private _authService: AuthService;
    private _conversationRestService: ConversationRestService

    constructor(authService: AuthService, conversationRestService: ConversationRestService) {
        this._authService = authService;
        this._conversationRestService = conversationRestService;
    }

    conversations: IConversationInfo[];
    activeConversation?: IConversationInfo = null;

    signOut() {
        this._authService.signOut();
    }

    ngOnInit() {
        this.conversations = [];

        this._conversationRestService
            .getConversations()
            .subscribe(
                x => this.conversations = x
            );
    }

    onConversationSelected(conversation: IConversationInfo) {
        this.activeConversation = conversation;
    }
}
