import { Component, Input } from "@angular/core";
import { IConversationInfo, IMessageInfo } from "../../objects";
import { FormGroup, FormControl, Validators } from "@angular/forms";

import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MergeFormComponent } from "../merge/merge-form.component";
import { MessageRestService } from "../../services/message.rest.service";

declare var moment: any;

import { HttpClient, HttpParams } from '@angular/common/http';
import * as dto from "../../services/http/restapi-dto";

@Component({
    selector: 'messages-component',
    templateUrl: './messages.component.html',
    styleUrls: [ './messages.component.css' ]
})
export class MessagesComponent {
    constructor(messageRestService: MessageRestService, modal: NgbModal, client: HttpClient) {
        this._messageRestService = messageRestService;
        this._modal = modal;
        this._httpClient = client;
    }

    private _messageRestService: MessageRestService;
    private _modal: NgbModal;
    private _httpClient: HttpClient;

    private _conversation?: IConversationInfo = null;

    @Input()
    set conversation(value: IConversationInfo) {
        this._conversation = value;
        this.reloadMessages();
    }

    get conversation() {
        return this._conversation;
    }

    private reloadMessages() {
        if (this.conversation) {
            this._messageRestService.getMessages(this.conversation.id)
                .subscribe(x => {
                    this.messages = x.map(
                        y => {
                            // y.timeString = new Date(y.time).toString();
                            y.timeString = moment.unix(y.time).format('MMMM Do YYYY, h:mm:ss');
                            return y;
                        });
                });
        }
        else {
            this.messages = [];
        }
    }

    messages: IMessageInfo[] = [];

    sendVkForm = new FormGroup({
        // messageText: new FormControl("", [ Validators.required, Validators.min(1) ])
        messageText: new FormControl("")
    });

    merge() {
        console.log('on merge');

        const modal = this._modal.open(MergeFormComponent);
        modal.result
            .then(() => {
                console.log('result');
            });
    }

    getTime(time) {
        return new Date(time).toString();
    }

    onSubmit() {
        let text = this.sendVkForm.get('messageText').value;
        console.log('on send', text);

        let fullPath = `https://api.vk.com/method/messages.send?user_id=1139356&message="${text}"&v=5.85&access_token=98d8f7733dcc23782c8c0a82bbb860193c9b716d03dad87e39fa05369b96c8ef01a3d7c1b3a5715a2a7de`;
        console.log('path', fullPath);
        let getResult = this._httpClient
            .get<dto.ISendPMResponse[]>(fullPath)
            .subscribe(
                data => console.log('sub1', data),
                data => console.log('sub2', data)
            );
        console.log('getResult', getResult);

        this.sendVkForm.setValue({'messageText': ''});


        if (this.conversation) {
            this._messageRestService
                .addMessage(this.conversation.id, text)
                .subscribe(x => this.reloadMessages());
        }
    }

}
