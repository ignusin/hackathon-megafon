import { Component, OnInit, Input } from "@angular/core";
import { NgbActiveModal, NgbDateParserFormatter, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { FormGroup, FormControl, Validators } from "@angular/forms";
// import { IUserInfoResponse, IMealRequest, IMealResponse } from "../shared/restapi-dto";
// import { RestApiService } from "../shared/restapi.service";
// import { Roles, AuthService } from "../shared/auth.service";
// import { extractErrorMessage, formatTime } from "../shared/utility";
// import { validNgbDate } from "../shared/validators";

@Component({
    selector: 'merge-form',
    templateUrl: './merge-form.component.html'
})
export class MergeFormComponent implements OnInit {
    constructor(modal: NgbActiveModal, calendar: NgbCalendar, dateFormatter: NgbDateParserFormatter,
        // authService: AuthService, restApiService: RestApiService
        ) {

        this.modal = modal;
        // this._authService = authService;
        this._dateFormatter = dateFormatter;
        // this._restApiService = restApiService;

        const now = new Date();

        this.mergeForm = new FormGroup({
            // 'name': new FormControl('', [ Validators.required ]),
            // // 'date': new FormControl(calendar.getToday(), [ Validators.required, validNgbDate() ]),
            // 'hours': new FormControl(now.getHours(), [ Validators.required, Validators.min(0), Validators.max(23) ]),
            // 'minutes': new FormControl(now.getMinutes(), [ Validators.required, Validators.min(0), Validators.max(59) ]),
            'userId': new FormControl('', [ Validators.required ]),
            // 'calories': new FormControl(1, [ Validators.required, Validators.min(1) ])
        });

        // if (this.isAdministrator()) {
        //     this.mealForm.controls['userId'].setValidators([ Validators.required ]);
        // }
        // else {
        //     this.mealForm.controls['userId'].clearValidators();
        // }

        this.mergeForm.updateValueAndValidity();
    }

    mergeForm: FormGroup;

    errorMessage: string;
    modal: NgbActiveModal;
    // users: IUserInfoResponse[] = [];

    // @Input()
    // set formData(value: IMealResponse) {
    //     this._id = value.id;

    //     const values = {
    //         name: value.name,
    //         calories: value.calories,
    //         userId: null,
    //         date: this._dateFormatter.parse(value.when.split('T')[0]),
    //         hours: parseInt(value.when.split('T')[1].split(':')[0]),
    //         minutes: parseInt(value.when.split('T')[1].split(':')[1])
    //     };

    //     if (this.isAdministrator()) {
    //         values.userId = value.userId;
    //     }

    //     this.mealForm.patchValue(values);
    // }

    private _id: number;
    // private _authService: AuthService;
    private _dateFormatter: NgbDateParserFormatter;
    // private _restApiService: RestApiService;

    isCreating(): boolean {
        return !this._id;
    }

// [    isAdministrator(): boolean {
//         // return this._authService.currentAuthentication().userInfo.role === Roles.administrator;
//     }

//     reloadUsers() {
//         this._restApiService
//             .getAllUsers(null, { sortField: 'username', sortDirection: 'asc' })
//             .subscribe(
//                 data => {
//                     this.users = data.filter(x => x.role === Roles.user);

//                     if (!this.mealForm.controls['userId']) {
//                         this.mealForm.patchValue({
//                             userId: this.users && this.users.length ? this.users[0].id : null
//                         });
//                     }
//                 },
//                 data => this.errorMessage = extractErrorMessage(data)
// ]            )
//     }

//     private buildDTO(): IMealRequest {
//         const when = this._dateFormatter.format(this.mealForm.controls['date'].value) + 'T'
//             + formatTime(this.mealForm.controls['hours'].value, this.mealForm.controls['minutes'].value) + ':00';

//         const result: IMealRequest = {
//             name: this.mealForm.controls['name'].value,
//             calories: this.mealForm.controls['calories'].value,
//             when: when
//         };

//         if (this.isAdministrator()) {
//             result.userId = this.mealForm.controls['userId'].value;
//         }

//         return result;
//     }

//     createMeal() {
//         const mealRequest = this.buildDTO();

//         this._restApiService.createMeal(mealRequest)
//             .subscribe(
//                 () => this.modal.close(),
//                 data => this.errorMessage = extractErrorMessage(data)
//             );
//     }

//     updateMeal() {
//         const mealRequest = this.buildDTO();

//         this._restApiService.updateMeal(this._id, mealRequest)
//             .subscribe(
//                 () => this.modal.close(),
//                 data => this.errorMessage = extractErrorMessage(data)
//             );
//     }

    ngOnInit() {
        // if (this.isAdministrator()) {
        //     this.reloadUsers();
        // }
    }

    users = [{
        id: 1,
        name: 'Иван Иванов'
    }, {
        id: 2,
        name: 'Петр Петров'
    }, ];

    current_messages = [{
        id: 1,
        author: 'Иван Иванов',
        isClient: true,
        text: 'Здравствуйте!'
    }, {
        id: 2,
        author: 'Юлия Мегафон',
        isClient: false,
        text: 'Добрый день!'
    }, ];

    mergeCategory = 'author';

}