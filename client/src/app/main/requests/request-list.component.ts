import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { IConversationInfo } from "../../objects";

@Component({
    selector: 'request-list-component',
    templateUrl: './request-list.component.html',
    styleUrls: [ './request-list.component.css' ]
})
export class RequestListComponent implements OnInit {
    private _items: IConversationInfo[];

    @Output() selected: EventEmitter<IConversationInfo> = new EventEmitter();

    @Input()
    set items(items: IConversationInfo[]) {
        this._items = items;
    }

    get items() {
        return this._items;
    }

    ngOnInit() {
        this.items = [];
    }

    onSelected(item: IConversationInfo) {
        this.selected.emit(item);
    }
}
