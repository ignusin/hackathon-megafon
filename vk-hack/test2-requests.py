import requests

apiAddress = 'https://api.vk.com/method/'
vkApiVersion = '5.85'
token = '98d8f7733dcc23782c8c0a82bbb860193c9b716d03dad87e39fa05369b96c8ef01a3d7c1b3a5715a2a7de'

getUserEndpoint = 'users.get'
getUserParams = {
    'user_ids': '72211',
    'fields': 'photo_id, verified, sex, bdate, city, country, home_town, has_photo, photo_max_orig, online, domain, has_mobile, contacts, site, education, universities, schools, status, last_seen, followers_count, common_count, occupation, nickname, relatives, relation, personal, connections, exports, activities, interests, music, movies, tv, books, games, about, quotes, can_post, can_see_all_posts, can_see_audio, can_write_private_message, can_send_friend_request, is_favorite, is_hidden_from_feed, timezone, screen_name, maiden_name, is_friend, friend_status, career, military, blacklisted, blacklisted_by_me',
    'name_case': 'Nom',
    'v': vkApiVersion,
    'access_token' : token
}
getUserResponse = requests.get(apiAddress + getUserEndpoint, params = getUserParams)

# print(getUserResponse.text)
print(getUserResponse.json)

# getSubscriptionsEndpoint = 'users.getSubscriptions'
# sendPMEndpoint = 'messages.send'
# postWallEndpoint = 'wall.post'

