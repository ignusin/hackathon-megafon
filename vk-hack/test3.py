import json
from pprint import pprint

with open('short_dataset.json', encoding='utf8') as f:
    data = json.load(f)

for item in data:
    # search by id
    id = item.get('id')
    # if exists, skip
    # else process

    # userInfo = vk/users.get

    # userSubscriptions = vk/users.getSubscriptions

    subsItems = userSubscriptions.get('items')
    subsStringsArray = list(map(lambda x: x.get('name'), subsItems))

    author = {
        # id serial
        'name': userInfo.get('first_name') + userInfo.get('last_name'),
        'first_name': userInfo.get('first_name'),
        'last_name': userInfo.get('last_name'),
        'sex': userInfo.get('sex'), # or from item.get('sex')
        'age': item.get('age'),
        'psycho_data': {
            'military': userInfo.get('military'),
            'university': userInfo.get('university'),
            'university_name': userInfo.get('university_name'),
            'faculty': userInfo.get('faculty'),
            'faculty_name': userInfo.get('faculty_name'),
            'graduation': userInfo.get('graduation'),
            'education_form': userInfo.get('education_form'),
            'education_status': userInfo.get('education_status'),
            'home_town': userInfo.get('home_town'),
            'relation': userInfo.get('relation'),
            'interests': userInfo.get('interests'),
            'music': userInfo.get('music'),
            'activities': userInfo.get('activities'),
            'movies': userInfo.get('movies'),
            'tv': userInfo.get('tv'),
            'books': userInfo.get('books'),
            'games': userInfo.get('games'),
            'universities': userInfo.get('universities'),
            'schools': userInfo.get('schools'),
            'about': userInfo.get('about'),
            'quotes': userInfo.get('quotes'),

            'subscriptions': subsStringsArray
        },
        'identifiers': {
            'mobile_phone': userInfo.get('mobile_phone'),
            'home_phone': userInfo.get('home_phone'),
            'skype': userInfo.get('skype'),
            'facebook': userInfo.get('facebook'),
            'facebook_name': userInfo.get('facebook_name'),
            'twitter': userInfo.get('twitter'),
            'instagram': userInfo.get('instagram'),
            'site': userInfo.get('site'),
            
            'vk_id': userInfo.get('id'),
        },
        'psycho_tags': []
    }

    pprint(author)
    return


# pprint(data[0])
