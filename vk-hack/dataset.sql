CREATE TABLE public.data
(
    id serial,
    old_id integer,
    hash text,
    idExternal text,
    timeCreate integer,
    title text,
    text text,
    hub text,
    url text,
    hubtype text,
    type text,
    author_fullname text,
    author_url text,
    author_sex text,
    author_age text,
    commentsCount integer,
    audienceCount integer,
    repostsCount integer,
    likesCount integer,
    er integer,
    viewsCount integer,
    duplicateCount integer,
    toneMark integer,
    country integer,
    region text,
    city text,
    language text,
    tags json,
    geo_type text,
    geo_geo_type text,
    geo_geo_coordinates text,
    geo_properties_external_id integer,
    geo_properties_id integer,
    geo_properties_source text,
    geo_properties_title text,
    geo_properties_address text,
    geo_properties_country text,
    geo_properties_region text,
    geo_properties_city text,
    geo_properties_matchForQuery integer
);


-- authors
create table authors (
    id serial primary key not null,
    vk_id text,
    name text not null,
    first_name text,
    last_name text,
    sex text,
    age integer,
    identifiers jsonb,
    psycho_data jsonb,
    psycho_tags jsonb
);

