# -*- coding: utf-8 -*-
import vk_api


def main():
    """ Пример получения последнего сообщения со стены """

    login, password = '+79994624272', 'mypassword'
    vk_session = vk_api.VkApi(login=login, token='98d8f7733dcc23782c8c0a82bbb860193c9b716d03dad87e39fa05369b96c8ef01a3d7c1b3a5715a2a7de')

    try:
        vk_session.auth(token_only=True)
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return

    vk = vk_session.get_api()

    """ VkApi.method позволяет выполнять запросы к API. В этом примере
        используется метод wall.get (https://vk.com/dev/wall.get) с параметром
        count = 1, т.е. мы получаем один последний пост со стены текущего
        пользователя.
    """
    response = vk.wall.get(count=1)  # Используем метод wall.get

    if response['items']:
        print(response['items'][0])


if __name__ == '__main__':
    main()
