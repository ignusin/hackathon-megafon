import db
import vk_requests

data = db.read_all_data()

for item in data:
    # search by id
    # item is tuple
    author_url = item[12]   # "http://vk.com/id479465599" or "http://vk.com/club3785"
    if not author_url[14:16] == 'id':
        # than it is a group => skip 
        continue
    vk_id = author_url[16:]
    author = db.get_author_by_vk_id(vk_id)
    
    # if exists, skip
    if author is not None:
        continue
    # else process

    userInfo = vk_requests.getUserInfo(vk_id)
    userSubscriptions = vk_requests.getUserSubscriptions(vk_id)

    # break
    if userSubscriptions is None:
        # it is possible for instance when user is deleted or banned
        subsStringsArray = []
    else:
        subsItems = userSubscriptions.get('items')
        subsStringsArray = list(map(lambda x: x.get('name'), subsItems))

    user_sex = userInfo.get('sex')
    if user_sex is None:
        user_sex = item[13] # author_sex
    age = item[14]  # author_age
    if type(age) is not int and (type(age) is str and not age.isnumeric()):
        age = None
    author = {
        # id serial
        'vk_id': vk_id,
        'name': userInfo.get('first_name') + ' ' + userInfo.get('last_name'),
        'first_name': userInfo.get('first_name'),
        'last_name': userInfo.get('last_name'),
        'sex': user_sex,
        'age': age,
        'psycho_data': {
            'military': userInfo.get('military'),
            'university': userInfo.get('university'),
            'university_name': userInfo.get('university_name'),
            'faculty': userInfo.get('faculty'),
            'faculty_name': userInfo.get('faculty_name'),
            'graduation': userInfo.get('graduation'),
            'education_form': userInfo.get('education_form'),
            'education_status': userInfo.get('education_status'),
            'home_town': userInfo.get('home_town'),
            'relation': userInfo.get('relation'),
            'interests': userInfo.get('interests'),
            'music': userInfo.get('music'),
            'activities': userInfo.get('activities'),
            'movies': userInfo.get('movies'),
            'tv': userInfo.get('tv'),
            'books': userInfo.get('books'),
            'games': userInfo.get('games'),
            'universities': userInfo.get('universities'),
            'schools': userInfo.get('schools'),
            'about': userInfo.get('about'),
            'quotes': userInfo.get('quotes'),

            'subscriptions': subsStringsArray
        },
        'identifiers': {
            'mobile_phone': userInfo.get('mobile_phone'),
            'home_phone': userInfo.get('home_phone'),
            'skype': userInfo.get('skype'),
            'facebook': userInfo.get('facebook'),
            'facebook_name': userInfo.get('facebook_name'),
            'twitter': userInfo.get('twitter'),
            'instagram': userInfo.get('instagram'),
            'site': userInfo.get('site'),
            
            'vk_id': userInfo.get('id'),
        },
        'psycho_tags': []
    }

    # store author
    db.store_author(author)
    # # stop to see how it works with one entry
    # break


