import psycopg2
import json

connection_string = "dbname=hdb user=hu password=123 host='ih1221245.vds.myihor.ru'"

def read_all_data():
    conn = psycopg2.connect(connection_string)
    cur = conn.cursor()
    # cur.execute("SELECT * FROM \"data\" where \"hub\"='vk.com' limit 2")
    cur.execute("SELECT * FROM \"data\" where \"hub\"='vk.com'")
    # auth_id = cur.fetchone()
    rows = cur.fetchall()
    # conn.commit()
    cur.close()
    conn.close()
    # print(rows)
    return rows

def get_author_by_vk_id(author_id):
    conn = psycopg2.connect(connection_string)
    cur = conn.cursor()
    cur.execute("SELECT * FROM \"authors\" where \"vk_id\"=%s limit 1", [author_id])
    author = cur.fetchone()
    # rows = cur.fetchall()
    # conn.commit()
    cur.close()
    conn.close()
    return author

def store_author(author):
    identifiers = author.get('identifiers')
    identifiers_json_string = json.dumps(identifiers)
    psycho_data = author.get('psycho_data')
    psycho_data_json_string = json.dumps(psycho_data)
    psycho_tags = author.get('psycho_tags')
    psycho_tags_json_string = json.dumps(psycho_tags)
    conn = psycopg2.connect(connection_string)
    cur = conn.cursor()
    cur.execute("insert into \"authors\" (\"vk_id\",\"name\",\"first_name\",\"last_name\",\"sex\",\"age\",\"identifiers\",\"psycho_data\",\"psycho_tags\") values (%s, %s, %s, %s, %s, %s, %s, %s, %s)", [author.get('vk_id'), author.get('name'), author.get('first_name'), author.get('last_name'), author.get('sex'), author.get('age'), identifiers_json_string, psycho_data_json_string, psycho_tags_json_string])
    # rows = cur.fetchall()
    conn.commit()
    cur.close()
    conn.close()
    # return rows

# result = read_all_data()
# print (result)