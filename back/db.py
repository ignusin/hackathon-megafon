import psycopg2

from config import config

def in_connection(query, params=None, fetcher=None):
    with psycopg2.connect(config['db']) as conn:
        with conn.cursor() as cur:
            if not params:
                cur.execute(query)
            else:
                cur.execute(query, params)

            if fetcher:
                result = fetcher(cur)

            conn.commit()
            return result if fetcher else None

def fetch_all(query, params=None):
    return in_connection(query, params, lambda cur: cur.fetchall())

def fetch_one(query, params=None):
    return in_connection(query, params, lambda cur: cur.fetchone())

def update(query, params=None):
    return in_connection(query, params)
