-- Table: public.tags

-- DROP TABLE public.tags;

CREATE TABLE public.tags
(
  id serial,
  name text NOT NULL,
  CONSTRAINT pk_tags PRIMARY KEY (id)
);


-- Table: public.users

-- DROP TABLE public.users;

CREATE TABLE public.users
(
  id serial,
  name text NOT NULL,
  password text NOT NULL,
  CONSTRAINT pk_users PRIMARY KEY (id)
);

-- Table: public."TagUsers"

-- DROP TABLE public."TagUsers";

CREATE TABLE public."TagUsers"
(
  id serial,
  "Id_Tag" integer,
  "Id_User" integer,
  CONSTRAINT pk_tagusers PRIMARY KEY (id),
  CONSTRAINT fk_tagusers_tags FOREIGN KEY ("Id_Tag")
      REFERENCES public.tags (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_tagusers_users FOREIGN KEY ("Id_User")
      REFERENCES public.users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- Index: public.fki_tagusers_tags

-- DROP INDEX public.fki_tagusers_tags;

CREATE INDEX fki_tagusers_tags
  ON public."TagUsers"
  USING btree
  ("Id_Tag");

-- Index: public.fki_tagusers_users

-- DROP INDEX public.fki_tagusers_users;

CREATE INDEX fki_tagusers_users
  ON public."TagUsers"
  USING btree
  ("Id_User");

CREATE UNIQUE INDEX "UX_TagUsers_Id_Tag_Id_User" ON "tags" ("Id_Tag", "Id_User");



create table "conversations" (
	"id"		serial	not null,
	"author_id"	int	not null,
	"time"		int	not null,
	"text"		text	not null
);

alter table "conversations"
	add constraint "pk_conversations"
	primary key ("id");

alter table "conversations"
	add constraint "fk_conversations_to_authors"
	foreign key ("author_id")
	references "authors" ("id");

create index "ix_conversations_author_id"
	on "conversations" ("author_id");

alter table "conversations" add "orig_id" text not null;

create index "ix_conversations_orig_id" on "conversations" ("orig_id");

create table "messages" (
	"id"			serial	not null,
	"orig_id"		text	not null,
	"conversation_id"	int 	not null,
	"author_id"		int	not null,
	"time"			int	not null,
	"text"			text	not null
);

alter table "messages"
	add constraint "pk_messages"
	primary key ("id");

alter table "messages"
	add constraint "fk_messages_to_conversations"
	foreign key ("conversation_id")
	references "conversations" ("id");

alter table "messages"
	add constraint "fk_messages_to_authors"
	foreign key ("author_id")
	references "authors" ("id");

create index "ix_messages_conversation_id"
	on "messages" ("conversation_id");

create index "ix_messages_author_id"
	on "messages" ("author_id");