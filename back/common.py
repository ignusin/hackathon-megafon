def mapi(fn, *iterables):
    return list(map(fn, *iterables))
