from flask import Flask, request, Response, jsonify
import logging

import api

from config import config

logging.basicConfig(filename='logger.log', level=logging.INFO, 
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
log = logging.getLogger('api.py')

app = Flask(__name__)

app.register_blueprint(api.auth.blueprint, url_prefix='/api')
app.register_blueprint(api.conversations.blueprint, url_prefix='/api')
app.register_blueprint(api.messages.blueprint, url_prefix='/api')
app.register_blueprint(api.tags.blueprint, url_prefix='/api')


if __name__ == '__main__':
    host = config['host']['url']
    port = config['host']['port']
    app.run(host=host, port=port, debug=True)
