import json
import psycopg2

from config import config

def load_users():
    with open('C:\\workdir\\H\\mf\\users.json', encoding='utf-8') as f:
        data = json.load(f)
        return data


def load_comments():
    with open('C:\\workdir\\H\\mf\\comments.json', encoding='utf-8') as f:
        data = json.load(f)
        return data


def index_users_by_id(raw):
    result = dict()
    for item in raw:
        if not item['id'] in result:
            result[item['id']] = item

    return result


def index_comments_by_from_id(raw):
    result = dict()
    for item in raw:
        if item['from_id'] not in result:
            result[item['from_id']] = []

        result[item['from_id']].append(item)

    return result


def index_comments_by_id(raw):
    result = dict()
    for item in raw:
        if not item['id'] in result:
            result[item['id']] = item

    return result


def index_comments_by_reply_to_id(raw):
    def try_parse_reply(item):
        text = item['text']
        if len(text) > 0 and text[0] == '[':
            cbi = text.find(']')
            if cbi < 0:
                return None

            reply_tok = text[1:cbi]
            rts0 = reply_tok.split('|')

            if len(rts0) != 2:
                return None

            rts1 = rts0[0].split(':')
            if len(rts1) != 2:
                return None

            rts2 = rts1[1][3:].split('_')

            to_user_display_name = rts0[1]
            from_user_id = rts1[0]
            group_id = rts2[0]
            to_message_id = rts2[1]
        
            return {
                'to_user_display_name': to_user_display_name,
                'from_user_id': from_user_id,
                'group_id': int(group_id),
                'to_message_id': int(to_message_id)
            }

        return None

    def clean_up_dups(index):
        to_remove = []
        for k, v in index.items():
            if len(v) > 1:
                to_remove.append(k)

        for k in to_remove:
            del index[k]

        for k in index.keys():
            index[k] = index[k][0]

    result = dict()
    for item in raw:
        reply_info = try_parse_reply(item)
        if not reply_info:
            continue

        if reply_info['to_message_id'] not in result:
            result[reply_info['to_message_id']] = []

        result[reply_info['to_message_id']].append( (reply_info, item) )

    clean_up_dups(result)

    return result


def detect_conversations(comments_by_id, comments_by_reply_to_id):
    def find_roots():
        idset = set()
        for v in comments_by_reply_to_id.values():
            idset.add(v[1]['id'])

        result = []
        for value in comments_by_reply_to_id.values():
            to_message_id = value[0]['to_message_id']

            if to_message_id not in idset \
                and to_message_id in comments_by_id:
                    result.append(to_message_id)
        
        return result
    
    roots = find_roots()

    queue = list()
    result = dict()

    for r in roots:
        queue.append( (r, r) )  # root id, last id
        result[r] = [r]
    
    while len(queue) > 0:
        x = queue.pop(0)

        if x[1] in comments_by_reply_to_id:
            next_id = comments_by_reply_to_id[x[1]][1]['id']
            result[x[0]].append(next_id)
            queue.append( (x[0], next_id) )
    
    return result


def extract_conversation_messages(conversations, comments_by_id):
    result = list()
    for k in conversations.keys():
        result.append(list(map(lambda x: comments_by_id[x], conversations[k])))

    return result   


def add_users_to_db(users_by_id):
    with psycopg2.connect(config['db']) as conn:
        with conn.cursor() as cur:
            for k, v in users_by_id.items():
                cur.execute('select "id" from "authors" where "vk_id" = %s', (str(k), ))
                one = cur.fetchone()
                if one:
                    continue
                
                cur.execute(
                    '''insert into "authors" ("vk_id", "name", "first_name", "last_name", "sex")
                        values (%s, %s, %s, %s, %s)''',
                    (str(k), v.get('last_name', '') + ' ' + v.get('first_name', ''),
                        v.get('first_name', ''), v.get('last_name', ''), v.get('sex'))
                )

                pass

            conn.commit()


def add_conversations_to_db(conversations, comments_by_id):
    with psycopg2.connect(config['db']) as conn:
        with conn.cursor() as cur:
            # for k in conversations.keys():
            #     c = comments_by_id[k]
            # 
            #     if 'from_id' not in c:
            #         continue
            # 
            #     cur.execute('select "id" from "authors" where "vk_id" = %s', (str(c['from_id']),))
            #     author_id = cur.fetchone()[0]
            # 
            #     cur.execute(
            #         '''insert into "conversations" ("orig_id", "author_id", "time", "text")
            #             values (%s, %s, %s, %s)''',
            #         (c.get('id', ''), author_id, c.get('date', 0), c.get('text', ''))
            #     )

            for v in conversations.values():
                if len(v) == 0:
                    continue

                c_orig_id = v[0]
                cur.execute('select "id" from "conversations" where "orig_id" = %s', (str(c_orig_id),))
                
                c_id = cur.fetchone()
                if not c_id:
                    continue

                for m_id in v:
                    m = comments_by_id[m_id]

                    cur.execute('select "id" from "authors" where "vk_id" = %s', (str(m['from_id']),))
                    author_id = cur.fetchone()[0]

                    if not author_id:
                        continue

                    cur.execute(
                        '''insert into "messages" ("orig_id", "conversation_id", "author_id", "time", "text")
                            values (%s, %s, %s, %s, %s)''',
                        (m.get('id', ''), c_id, author_id, m.get('date', 0), m.get('text', ''))
                    )

                    print('message added')

                print('conversation fully added')

            conn.commit()


def main():
    users_by_id = index_users_by_id(load_users())
    
    comments = load_comments()
    comments_by_id = index_comments_by_id(comments)
    comments_by_from_id = index_comments_by_from_id(comments)
    comments_by_reply_to_id = index_comments_by_reply_to_id(comments)

    conversations = detect_conversations(comments_by_id, comments_by_reply_to_id)
    conversation_messages = extract_conversation_messages(conversations, comments_by_id)

    # with open('C:\\workdir\\H\\mf\\comments_index.json', mode='w', encoding='utf-8') as f:
    #     json.dump(comments_by_id, f, ensure_ascii=False)
    # 
    # with open('C:\\workdir\\H\\mf\\conversation_message_ids.json', mode='w', encoding='utf-8') as f:
    #     json.dump(conversations, f, ensure_ascii=False)

    # add_users_to_db(users_by_id)
    add_conversations_to_db(conversations, comments_by_id)

    print('Done')

if __name__ == '__main__':
    main()