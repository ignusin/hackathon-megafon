from flask import jsonify, request, Blueprint

from api.decorators import authenticated
from common import mapi
import queries

blueprint = Blueprint('messages', __name__)

@blueprint.route('/messages/<c_id>', methods=['POST'])
@authenticated
def add_message(c_id):
    msg = request.json.get('message')
    queries.add_conversation_message(c_id, msg)

    return jsonify({ 'success': True })

@blueprint.route('/messages/<c_id>', methods=['GET'])
@authenticated
def get_messages(c_id):
    messages = queries.get_conversation_messages(c_id)
    return jsonify(
        mapi(
            lambda x: { 'id': x[0], 'time': x[1], 'text': x[2], 'clientId': x[3], 'clientName': x[4] },
            messages
        )
    )