from flask import jsonify, request, Blueprint

import queries

from api.decorators import authenticated
from common import mapi

__tag_extractor = lambda x: { 'id': x[0], 'name': x[1] }

blueprint = Blueprint('tags', __name__)

@blueprint.route('/tags', methods=['GET'])
@authenticated
def get_tags():
    return jsonify(mapi(__tag_extractor, queries.find_all_tags()))

@blueprint.route('/tags/my', methods=['GET'])
@authenticated
def get_tags_for_user():
    return jsonify(mapi(__tag_extractor, queries.find_all_tags_by_user_id(request.user_id)))

@blueprint.route('/tags/my', methods=['POST'])
@authenticated
def add_tag_to_user():
    queries.add_tag_to_user(request.json.tag_id, request.user_id)
    return jsonify()

@blueprint.route('/tags/my/<tag_id>', methods=['DELETE'])
@authenticated
def remove_tag_from_user(tag_id):
    queries.remove_tag_from_user(tag_id, request.user_id)
