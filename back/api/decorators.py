from flask import jsonify, request
from functools import wraps

def authenticated(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        user_id = request.cookies.get('userId')
        if not user_id:
            response = jsonify({ 'error': 'Not authorized' })
            response.status_code = 401
            return response

        request.user_id = user_id
        result = func(*args, **kwargs)

        return result
    
    return wrapper