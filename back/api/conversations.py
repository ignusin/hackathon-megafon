from flask import jsonify, request, Blueprint

from api.decorators import authenticated
from common import mapi
import queries

blueprint = Blueprint('conversations', __name__)

@blueprint.route('/conversations', methods=['GET'])
@authenticated
def get_conversations():
    conversations = queries.get_conversations()
    return jsonify(
        mapi(
            lambda x: { 'id': x[0], 'time': x[1], 'text': x[2], 'clientId': x[3], 'clientName': x[4] },
            conversations
        )
    )