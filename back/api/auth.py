from flask import jsonify, request, Blueprint

import queries


blueprint = Blueprint('auth', __name__)

@blueprint.route('/auth', methods=['POST'])
def authenticate():
    req = request.json
    auth_id = queries.find_user_id_by_creds(req.get('username'), req.get('password'))

    return jsonify({ 'id': auth_id })