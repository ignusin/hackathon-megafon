import db
import time

def find_user_id_by_creds(name, password):
    return db.fetch_one('SELECT id FROM users WHERE name=%s and password=%s', (name, password))

def find_all_tags():
    return db.fetch_all('SELECT "id", "name" FROM "tags" ORDER BY "name"')

def find_all_tags_by_user_id(user_id):
    return db.fetch_all(
        '''SELECT t."id", t."name"
            FROM "TagUsers" tu
            INNER JOIN "tags" t ON tu."Id_Tag" = t."id"
            WHERE tu."Id_User" = %s
        ''',
        user_id
    )

def add_tag_to_user(tag_id, user_id):
    db.update('INSERT INTO "TagUsers" ("Id_Tag", "Id_User") VALUES (%s, %s)', (tag_id, user_id))

def remove_tag_from_user(tag_id, user_id):
    db.update('DELETE FROM "TagUsers" WHERE "Id_Tag" = %s AND "Id_User" = %s', (tag_id, user_id))

def get_conversations():
    return db.fetch_all(
        '''SELECT c."id", c."time", c."text", a."id" as "author_id", a."name" as "author_name"
            FROM "conversations" c
            INNER JOIN "authors" a ON c."author_id" = a."id"
            ORDER BY c."time" DESC LIMIT 10'''
    )

def add_conversation_message(c_id, msg):
    db.update(
        '''INSERT INTO "messages" ("time", "text", "author_id", "conversation_id", "orig_id")
            VALUES (%s, %s, %s, %s, %s)''',
        (int(time.time()), msg, 188, c_id, 'NO')
    )

def get_conversation_messages(c_id):
    return db.fetch_all(
        '''SELECT m."id", m."time", m."text", a."id" as "author_id", a."name" as "author_name"
            FROM "messages" m
            INNER JOIN "authors" a ON m."author_id" = a."id"
            WHERE m."conversation_id" = %s
            ORDER BY m."time" ASC''',
        (c_id,)
    )
